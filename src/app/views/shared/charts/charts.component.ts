import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
})
export class ChartsComponent implements OnInit {
  @Input() chartTitle: string;
  @Input() chartLabels: Label[];
  @Input() chartData: ChartDataSets[];
  @Input() chartType: ChartType;
  @Input() chartOptions: ChartOptions[];
  @Input() chartColors: Array<any> = [];
  @Input() chartLegend: boolean;
  @Input() width: number;
  @Input() height: number;

  constructor() {}

  ngOnInit(): void {
    // console.log('this.chartData', this.chartData);
    // console.log('this.chartType', this.chartType);
    // console.log('this.chartOptions', this.chartOptions);
    // console.log('this.chartLegend', this.chartLegend);
    // console.log('this.width', this.width);
    // console.log('this.height', this.height);
  }
}

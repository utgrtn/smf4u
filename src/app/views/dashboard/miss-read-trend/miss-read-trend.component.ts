import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import moment from 'moment';

@Component({
  selector: 'app-miss-read-trend',
  templateUrl: './miss-read-trend.component.html',
  styleUrls: ['./miss-read-trend.component.scss'],
})
export class MissReadTrendComponent implements OnInit {
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  // prepare line chart data

  public lineChartData: any[] = [
    { data: [], label: 'Unknown / Missed Reads', fill: false },
  ];
  public lineChartLabels = [];
  public lineChartType = 'line';
  public lineChartLegend = true;
  public lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        anchor: 'center',
        align: 'top',
      },
    },
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#81d4fa',
      backgroundColor: '#00bcd4',
    },
  ];
  constructor(private dashboardService: DashboardService) {}
  updateChart(): void {
    console.log(this.charts);
    this.chart.update();
    this.charts.forEach((child) => {
      child.chart.update();
    });
  }
  ngOnInit(): void {
    this.dashboardService.getMissReadTrendData().subscribe({
      next: (trendItems) => {
        // sort the data by date
        trendItems.sort((a, b) => {
          return moment(a.INSERT_DATE) < moment(b.INSERT_DATE)
            ? -1
            : moment(a.INSERT_DATE) > moment(b.INSERT_DATE);
        });
        trendItems.forEach((li) => {
          this.lineChartData[0].data.push(li.MISSED_READS);
          this.lineChartLabels.push(moment(li.INSERT_DATE).format('L'));
        });
      },
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissReadTrendComponent } from './miss-read-trend.component';

describe('MissReadTrendComponent', () => {
  let component: MissReadTrendComponent;
  let fixture: ComponentFixture<MissReadTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissReadTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissReadTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatTableDataSource } from '@angular/material/table';
import {
  MissreadDetailDataSource,
  MissreadDetailItem,
} from './missread-detail-datasource';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-missread-detail',
  templateUrl: './missread-detail.component.html',
  styleUrls: ['./missread-detail.component.scss'],
})
export class MissreadDetailComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<MissreadDetailItem>;
  // dataSource: MissreadDetailDataSource;
  ELEMENT_DATA: any [];
  displayedColumns: string[] = ['custid', 'cust-class', 'meterid', 'order-type', 'bill-cycle', 'last-read-date', 'activation-status'];
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    const response = this.dashboardService.getMissedReadData();
    response.subscribe((d) => {
      this.dataSource.data = d as any[];
      this.dataSource.sort = this.sort;
      // console.log(d);
    });
    // console.log(this.dataSource.data);
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}

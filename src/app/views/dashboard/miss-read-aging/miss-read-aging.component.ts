import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { Label, Color } from 'ng2-charts';
@Component({
  selector: 'app-miss-read-aging',
  templateUrl: './miss-read-aging.component.html',
  styleUrls: ['./miss-read-aging.component.scss'],
})
export class MissReadAgingComponent implements OnInit {
  public barChartData: any[] = [{ data: [], label: 'Days' }];
  public barChartLabels = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    plugins: {
      datalabels: {
        anchor: 'center',
        align: 'top',
      },
    },
  };
  public barChartColors: Color[] = [
    { backgroundColor: '#81d4fa' }, // #5DB4EE
    { backgroundColor: '#00bcd4' },
  ];
  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.dashboardService.getMissReadAgingData().subscribe({
      next: (trendItems) => {
        // console.log(trendItems, Object.values(trendItems[0]), Object.keys(trendItems[0]));
        this.barChartData[0].data = Object.values(trendItems[0]);
        this.barChartLabels = Object.keys(trendItems[0]).map(e => e.slice(3).replace('_', ' ').replace('_', '-'));
      },
    });
    // console.log(this.barChartData);
  }
}

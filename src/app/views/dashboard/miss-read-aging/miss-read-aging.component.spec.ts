import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissReadAgingComponent } from './miss-read-aging.component';

describe('MissReadAgingComponent', () => {
  let component: MissReadAgingComponent;
  let fixture: ComponentFixture<MissReadAgingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissReadAgingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissReadAgingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

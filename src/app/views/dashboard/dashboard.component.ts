import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
  public icon: string;
  public title: string;
  public value: number;
  public color: string;
  public isNumber: boolean;
  public isPercent: boolean;
  public readRatesThreshold = 98.5;
  public meterThreshold = 10000;

  public oKPIData = [];
  constructor(private dashboardService: DashboardService) {}
  updateChart(): void {
    // console.log(this.charts);
    this.charts.forEach((child) => {
      child.chart.update();
    });
  }
  ngOnInit(): void {
    const meterResponse = this.dashboardService.getMeterCount();
    meterResponse.subscribe((d) => {
      // console.log(d);
      this.oKPIData.push(
        {
          icon: 'av_timer',
          title: 'Total Meters',
          value: d[0].tot_meter_count,
          color: d[0].tot_meter_count < this.meterThreshold ? '#f44336' : '#039be5',
          isNumber: true,
          tooltip: `Theshold value is ${this.meterThreshold}`
        },
        {
          icon: 'av_timer',
          title: 'Active Meters',
          value: d[0].active_meter_count,
          color: d[0].active_meter_count < this.meterThreshold ? '#f44336' : '#039be5',
          isNumber: true,
          tooltip: `Theshold value is ${this.meterThreshold}`
        },
        {
          icon: 'supervised_user_circle',
          title: 'Total Customers',
          value: d[0].tot_customer_count,
          color: d[0].tot_customer_count < this.meterThreshold ? '#f44336' : '#ffb300',
          isNumber: true,
          tooltip: `Theshold value is ${this.meterThreshold}`
        }
      );
    });
    const irResponse = this.dashboardService.getIRRate();
    irResponse.subscribe((d) => {
      // console.log(d);
      this.oKPIData.push(
        {
          icon: 'rate_review',
          title: 'Interval Read Rate',
          value: d[0].interval_read_date,
          color: d[0].interval_read_date < this.readRatesThreshold ? '#f44336' : '#388e3c',
          // isNumber: false,
          isPercent: true,
          tooltip: `Theshold value is ${this.readRatesThreshold}`
        },
        {
          icon: 'rate_review',
          title: 'Register Read Rate',
          value: d[0].interval_register_date,
          color: d[0].interval_register_date < this.readRatesThreshold ? '#f44336' : '#388e3c',
          // isNumber: false,
          isPercent: true,
          tooltip: `Theshold value is ${this.readRatesThreshold}`
        }
      );
    });
  }
}

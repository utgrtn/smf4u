import { Input, Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

declare const particlesJS: any;
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  @Input() error: string | null;
  @Output() submitEM = new EventEmitter();

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  submit(): void {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
    }
  }

  constructor(private router: Router) {}

  signIn(): void {
    console.log(`User logger in`);
    this.router.navigateByUrl('smf');
  }

  ngOnInit(): void {
    particlesJS.load(
      'particles-js',
      '../../../../assets/particles.json',
      () => {
        console.log('callback - particles.js config loaded');
      }
    );
  }
}

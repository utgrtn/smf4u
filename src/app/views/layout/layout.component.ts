import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  ViewChildren,
  QueryList,
  ViewChild,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ThemingService } from 'src/app/services/theming.service';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { MatSidenavModule } from '@angular/material/sidenav';
import moment from 'moment';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  collapsedNav: boolean;
  mobileQuery: MediaQueryList;
  themes: string[];
  LastAccessTime = moment().startOf('day').fromNow();

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
  @ViewChild('snav') snav: MatSidenavModule;
  // tslint:disable-next-line: variable-name
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private theming: ThemingService,
    private router: Router
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  updateChart(): void {
    this.charts.forEach((child) => {
      child.chart.update();
    });
  }
  changeTheme(theme: string, sidenav: any): void {
    Chart.defaults.global.defaultFontColor = theme === 'light-theme' ? 'black' : 'white';
    this.collapsedNav = !this.collapsedNav;
    // console.log(this.charts);
    // sidenav.toggle();
    this.charts.forEach((child) => {
      child.chart.update();
    });
    this.theming.theme.next(theme);
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  signOut(): void {
    console.log(`User logger out`);
    this.router.navigateByUrl('/');
  }
  ngOnInit(): void {
    this.themes = this.theming.themes;
  }
}

import { Component, OnInit, AfterViewInit, ViewChildren, QueryList, ContentChild, TemplateRef } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { TitleCasePipe } from '@angular/common';
import { AttrChartsComponent } from './attr-charts/attr-charts.component';

@Component({
  selector: 'app-mdrecon',
  templateUrl: './mdrecon.component.html',
  styleUrls: ['./mdrecon.component.scss'],
  providers: [TitleCasePipe],
})
export class MdreconComponent implements OnInit, AfterViewInit {
  public icon: string;
  public title: string;
  public value: number;
  public color: string;
  public isNumber: boolean;
  @ContentChild(TemplateRef) inputTemplate: TemplateRef<any>;
  @ViewChildren(AttrChartsComponent) inputComponents: QueryList<any>;

  public oKPIData = [];

  constructor(
    private dashboardService: DashboardService,
    private titlecasePipe: TitleCasePipe
  ) {}

  ngOnInit(): void {
    this.dashboardService.getMDRSummaryData().subscribe({
      next: (resp) => {
        // console.log(resp);
        const kpiData = resp.filter((el) => {
          return el.METRICS_ID === 'KPI';
        });
        // console.log(kpiData);
        kpiData.forEach((e) => {
          this.oKPIData.push({
            icon: 'av_timer',
            title: (e.STATUS.split('_').join(' ')),
            value: e.COUNT,
            color: e.COUNT > 98 ? 'green' : '#f44336',
            isNumber: true,
          });
        });
      },
    });

    // // get data for MUDR detail table
    // this.dashboardService.getMDRDetailData().subscribe({
    //   next: (resp) => {
    //     console.log(resp);
    //     // this.dataSource.data = resp;
    //     // this.dataSource.sort = this.sort;
    //     // resp.forEach(el => {
    //     //   this.chartData.push(el.cnt);
    //     //   this.chartLabels.push(el.SUB_METRICS_ID);
    //     // });
    //   },
    // });
  }

  ngAfterViewInit(): any {
    // THIS SHOULD NOT BE EMPTY
    // console.log('I work');
    // this.inputComponents.first.subscribe(c =>
    //   console.log('Children: ', this.inputComponents.toArray()));
    // console.log(this.inputComponents.toArray());
  }
}

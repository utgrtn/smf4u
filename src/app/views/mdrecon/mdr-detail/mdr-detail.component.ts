import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { MultiDataSet, Label, BaseChartDirective, Color } from 'ng2-charts';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';

interface IPoint {
  _index: number;
}
@Component({
  selector: 'app-mdr-detail',
  templateUrl: './mdr-detail.component.html',
  styleUrls: ['./mdr-detail.component.scss'],
})
export class MdrDetailComponent implements OnInit {
  ELEMENT_DATA: any[];
  displayedColumns: string[] = [
    'acc-no',
    'commodity',
    'meter-status',
    'meter-no',
    // 'status'
  ];
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);

  @ViewChild(BaseChartDirective) charts: BaseChartDirective;
  public chartTitle = 'MDRecon Summary';
  public chartData: any[] = [
    {
      data: [],
      label: 'Missing Meters',
      // barPercentage: 1,
      // categoryPercentage: 0.5,
    },
  ];
  // public chartData = [];
  public chartLabels = [];
  public chartOptions: ChartOptions = {
    maintainAspectRatio: false,
    responsive: false,
    // This chart will not respond to mousemove, etc
    events: ['click'],
    legend: {
      position: 'bottom'
    },
    // We use these empty structures as placeholders for dynamic theming.
    // scales: {
    //   yAxes: [
    //     {
    //       gridLines: {
    //         display: false,
    //       },
    //     },
    //   ],
    //   xAxes: [
    //     {
    //       gridLines: {
    //         display: false,
    //       },
    //       ticks: {
    //         beginAtZero: false,
    //         // stepValue: 10,
    //         // steps: 20,
    //         // max: 50,
    //       },
    //     },
    //   ],
    // },
    plugins: {
      datalabels: {
        anchor: 'center',
        align: 'center',
      },
    },
  };
  public chartType = 'horizontalBar';
  public chartLegend = true;
  // public chartData: MultiDataSet = [[]];
  // public chartPlugins = [pluginDataLabels];
  public chartColors: Array<any> = [
    { backgroundColor: ['rgba(55,205,255,0.3)', 'rgba(255,0,0,0.3)', 'rgba(255,155,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)', 'rgba(255,0,255,0.3)', '#e12021'] },
  ];

  public showFilteredFlag = false;
  public filterValue: string;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private dashboardService: DashboardService) { }
  applyFilter(event: Event): void {
    this.filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = this.filterValue.trim().toLowerCase();
  }
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    const activePoints = this.charts.chart.getElementAtEvent(event);
    const firstPoint = activePoints[0] as IPoint;
    if (firstPoint !== undefined) {
      this.filterValue = this.charts.chart.data.labels[firstPoint._index] as string;
      this.dataSource.filter = this.filterValue.replace(/ /g, '_').trim().toLowerCase();
      // console.log(firstPoint, this.charts.chart.data.labels, this.filterValue);
      this.showFilteredFlag = true;
    } else {
      this.dataSource.filter = '';
      this.showFilteredFlag = false;
    }

    // const value = this.charts.chart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
    // alert(label + ': ' + value);
  }
  ngOnInit(): void {
    // get data for MUDR detail table
    this.dashboardService.getMDRDetailData().subscribe({
      next: (resp) => {
        // console.log(resp);
        this.dataSource.data = resp;
        this.dataSource.sort = this.sort;
        // resp.forEach(el => {
        //   this.chartData.push(el.cnt);
        //   this.chartLabels.push(el.SUB_METRICS_ID);
        // });
      },
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.filterPredicate = (data: any, filterValue: string) => {
      return data.STATUS_MISSING_METERS
        .trim()
        .toLocaleLowerCase() === filterValue;
    };

    this.dashboardService.getMDRDetailCountData().subscribe({
      next: (resp) => {
        // console.log(resp);
        // this.dataSource.data = resp;
        // this.dataSource.sort = this.sort;
        resp.forEach(el => {
          if (el.STATUS_MISSING_METERS !== 'MATCHING_METERS') {
            this.chartData[0].data.push(el.COUNT_MISSING);
            this.chartLabels.push(el.STATUS_MISSING_METERS.replace(/_/g, ' '));
          }
          // this.chartData.push(el.COUNT_MISSING);
          //   this.chartData.push(el.cnt);
          //   this.chartLabels.push(el.SUB_METRICS_ID);
        });
      },
    });
    // console.log(this.chartData);
  }
}

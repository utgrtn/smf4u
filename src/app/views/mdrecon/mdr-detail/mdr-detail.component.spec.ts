import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdrDetailComponent } from './mdr-detail.component';

describe('MdrDetailComponent', () => {
  let component: MdrDetailComponent;
  let fixture: ComponentFixture<MdrDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdrDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdrDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { TitleCasePipe } from '@angular/common';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-power-load-status',
  templateUrl: './power-load-status.component.html',
  styleUrls: ['./power-load-status.component.scss'],
  providers: [TitleCasePipe],
})
export class PowerLoadStatusComponent implements OnInit {
  public chartFilterValue: string;
  public isDataAvailable: boolean;
  public chartType = 'pie';
  public chartLegend = true;
  public chartLabels: Label[] = [];
  public chartData: MultiDataSet = [[]];
  public chartPlugins = [pluginDataLabels];
  public chartColors: Array<any> = [
    {
      backgroundColor: [
        '#67C1FD',
        '#FDDA83',
        '#FD89A2',
        'rgba(255,0,0,0.3)',
        'rgba(0,255,0,0.3)',
        'rgba(0,0,255,0.3)',
      ],
    },
  ];
  public chartOptions: ChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: true,
      position: 'right',
      labels: {
        fontColor: '#f57c00'
      },
    },
    // We use these empty structures as placeholders for dynamic theming.
    // scales: {
    //   yAxes: [{}],
    //   xAxes: [
    //     {
    //       ticks: {
    //         beginAtZero: true,
    //         // stepValue: 10,
    //         // steps: 20,
    //         // max: 50,
    //       },
    //     },
    //   ],
    // },
    plugins: {
      datalabels: {
        anchor: 'center',
        align: 'center',
      },
    },
  };

  public matchingData = [];
  public mismatchData = [];
  public labelData = [];

  constructor(
    private dashboardService: DashboardService,
    private titlecasePipe: TitleCasePipe
  ) {}

  ngOnInit(): void {
    this.dashboardService.getMDRSummaryData().subscribe({
      next: (resp) => {
        const plStatusData = resp.filter((el) => {
          return el.METRICS_ID === 'LOAD_POWER_STATUS';
        });
        // console.log(plStatusData);
        plStatusData.forEach((el) => {
          this.chartData[0].push(el.COUNT);
          this.chartLabels.push(
            this.titlecasePipe.transform(
              el.SUB_METRICS_ID.replace('_', ' ') + ' ' + el.STATUS
            )
          );
        });
        this.isDataAvailable = true;
        // const kpiData = resp.filter((el) => {
        //   return el.METRICS_ID === 'KPI';
        // });
        // console.log(kpiData);
        // // kpiData.forEach((el) => {
        // //   this.chartData.push(el.COUNT);
        // //   this.chartLabels.push(el.STATUS);
        // // });

        // const attribData = resp.filter((el) => {
        //   return (
        //     el.METRICS_ID !== 'KPI' && el.METRICS_ID !== 'POWER_LOAD_STATUS'
        //   );
        // });
        // console.log(attribData);
        // attribData.forEach((el, i) => {
        //   if (el.STATUS === 'MATCHING') {
        //     this.matchingData.push(el.COUNT);
        //   } else {
        //     this.mismatchData.push(el.COUNT);
        //   }
        //   if (i % 2) {
        //     this.labelData.push(el.METRICS_ID);
        //   }
        // });
        // this.barChartData.push({
        //   data: this.matchingData,
        //   label: 'MATCHING',
        //   backgroundColor: '#738830'
        // });
        // this.barChartData.push({
        //   data: this.mismatchData,
        //   label: 'MISMATCH',
        //   // backgroundColor: 'blue'
        // });
        // // this.barChartLabels.push(this.labelData);
        // this.barChartData.pop();
        // console.log(this.barChartData, this.barChartLabels);
      },
    });
  }
}

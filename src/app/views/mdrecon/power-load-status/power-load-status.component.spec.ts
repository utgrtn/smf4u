import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerLoadStatusComponent } from './power-load-status.component';

describe('PowerLoadStatusComponent', () => {
  let component: PowerLoadStatusComponent;
  let fixture: ComponentFixture<PowerLoadStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerLoadStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerLoadStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { DashboardService } from 'src/app/services/dashboard.service';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { groupBy } from '../../../../../node_modules/lodash/fp/groupBy';
import _ from 'lodash';
import { fadeInItems } from '@angular/material/menu';

@Component({
  selector: 'app-attr-charts',
  templateUrl: './attr-charts.component.html',
  styleUrls: ['./attr-charts.component.scss'],
})
export class AttrChartsComponent implements OnInit {
  public chartTitle = 'Rate Attributes';
  public chartData: ChartDataSets[] = [
    {
      data: [65, 59],
      label: 'Register Attributes',
      barPercentage: 1,
      categoryPercentage: 0.5,
    },
  ];
  public chartLabels = ['Mismatch', 'Match'];
  public chartOptions: ChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      yAxes: [
        {
          gridLines: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            beginAtZero: true,
            // stepValue: 10,
            // steps: 20,
            // max: 50,
          },
        },
      ],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        align: 'right',
      },
    },
  };
  public chartType = 'horizontalBar';
  public chartLegend = false;
  // public chartData: MultiDataSet = [[]];
  // public chartPlugins = [pluginDataLabels];

  public chartColors1: Array<any> = [
    { backgroundColor: ['rgba(55,205,255,0.3)', 'rgba(255,0,0,0.3)', 'rgba(255,155,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)', 'rgba(255,0,255,0.3)', '#e12021'] },
  ];

  public chartColors: Array<any> = [
    { backgroundColor: 'rgba(0,255,0,0.4)' },
    { backgroundColor: 'rgba(255,0,0,0.3)' },
  ];

  public matchingData = [];
  public mismatchData = [];
  public labelData = [];
  public datasource = [];
  height = 100;
  public oData = {
    data: {
      kpi: [
        {
          count: 1,
          name: 'Matching SAP',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing SAP',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing SAP MDM',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing SAP HES',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing HES',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing MDM',
          sub_metric_id: 'KPI',
        },
        {
          count: 1,
          name: 'Missing MDM HES',
          sub_metric_id: 'KPI',
        },
      ],
      attributes: {
        rate: {
          name: 'Rate Attributes',
          mismatch: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
          match: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
        },
        register: {
          name: 'Register Attributes',
          mismatch: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
          match: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
        },
        location: {
          name: 'Register Attributes',
          mismatch: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
          match: {
            count: 1,
            sub_metric_id: 'RATE_DATE',
          },
        },
      },
      powerLoadStatus: {
        plsMatch: {
          name: 'Power and Load Status Match',
          count: 1,
        },
        plsMismatch: {
          name: 'Power and Load Status Mismatch',
          count: 1,
        },
        psMismatch: {
          name: 'Power Status Mismatch',
          count: 1,
        },
        lsMismatch: {
          name: 'Load Status Mismatch',
          count: 1,
        },
        vdMismatch: {
          name: 'Virtual Disconnection Mismatch',
          count: 1,
        },
      },
    },
  };
  constructor(
    private dashboardService: DashboardService,
    private titlecasePipe: TitleCasePipe
  ) {}

  ngOnInit(): void {
    // console.log(this.oData.data.attributes);
    // const attribData = this.oData.data.attributes;
    // for (const [key, value] of Object.entries(this.oData.data.attributes)) {
    //   this.datasource.push({
    //     chartTitle: key,
    //     chartData: [
    //       {
    //         data: [attribData[key].match.count, attribData[key].mismatch.count],
    //         label: key,
    //       },
    //     ],
    //     chartLabels: ['match', 'mismatch'],
    //     chartType: 'horizontalBar',
    //     chartLegend: true,
    //   });
    // }
    // Live API
    this.dashboardService.getMDRSummaryData().subscribe({
      next: (resp) => {
        let attribData = resp.filter((el) => {
          return (
            el.METRICS_ID !== 'KPI' && el.METRICS_ID !== 'LOAD_POWER_STATUS'
          );
        });
        // console.log(attribData);
        attribData = _.groupBy(attribData, 'METRICS_ID');
        // console.log(attribData, JSON.stringify(attribData));
        for (const [key, value] of Object.entries(attribData)) {
          const tempData = [];
          const tempLabel = [];
          attribData[key].forEach((e) => {
            tempData.push(e.COUNT);
            tempLabel.push(
              this.titlecasePipe.transform(
                e.SUB_METRICS_ID.replace(/_/g, ' ') + ' ' + e.STATUS
              )
            );
          });
          this.datasource.push({
            chartTitle:
              key.charAt(0) + key.slice(1).toLowerCase().replace(/_/g, ' ') + ' Attributes',
            chartData: [
              {
                data: tempData,
                label: key,
              },
            ],
            chartLabels: tempLabel,
          });
        }
        // console.log(this.datasource);
      },
    });
    // console.log('this.chartData', this.chartData);
    // console.log('this.chartType', this.chartType);
    // console.log('this.chartOptions', this.chartOptions);
    // console.log('this.chartLegend', this.chartLegend);
  }
}

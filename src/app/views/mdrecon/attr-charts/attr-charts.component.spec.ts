import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttrChartsComponent } from './attr-charts.component';

describe('AttrChartsComponent', () => {
  let component: AttrChartsComponent;
  let fixture: ComponentFixture<AttrChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttrChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttrChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdreconComponent } from './mdrecon.component';

describe('MdreconComponent', () => {
  let component: MdreconComponent;
  let fixture: ComponentFixture<MdreconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdreconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdreconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

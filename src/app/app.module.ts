import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableExporterModule } from 'mat-table-exporter';
import { ChartsModule } from 'ng2-charts';

// View Components
import { LayoutComponent } from './views/layout/layout.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { MdreconComponent } from './views/mdrecon/mdrecon.component';
import { MissreadDetailComponent } from './views/dashboard/missread-detail/missread-detail.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { KpiCardComponent } from './views/shared/kpi-card/kpi-card.component';
import { MissReadAgingComponent } from './views/dashboard/miss-read-aging/miss-read-aging.component';
import { MissReadTrendComponent } from './views/dashboard/miss-read-trend/miss-read-trend.component';
import { PowerLoadStatusComponent } from './views/mdrecon/power-load-status/power-load-status.component';
import { ChartsComponent } from './views/shared/charts/charts.component';
import { AttrChartsComponent } from './views/mdrecon/attr-charts/attr-charts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Services
import { DashboardService } from './services/dashboard.service';
import { ThemingService } from './services/theming.service';
import { MdrDetailComponent } from './views/mdrecon/mdr-detail/mdr-detail.component';
import { SignInComponent } from './views/layout/sign-in/sign-in.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    DashboardComponent,
    MdreconComponent,
    MissreadDetailComponent,
    KpiCardComponent,
    MissReadAgingComponent,
    MissReadTrendComponent,
    PowerLoadStatusComponent,
    ChartsComponent,
    AttrChartsComponent,
    MdrDetailComponent,
    SignInComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    MatTableExporterModule,
    ChartsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  providers: [DashboardService, ThemingService],
  bootstrap: [AppComponent],
})
export class AppModule {}

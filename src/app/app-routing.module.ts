import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { MdreconComponent } from './views/mdrecon/mdrecon.component';
import { Spinkit } from 'ng-http-loader';
import { SignInComponent } from './views/layout/sign-in/sign-in.component';
import { LayoutComponent } from './views/layout/layout.component';

const routes: Routes = [
  {
    path: 'smf',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      { path: 'mdrecon', component: MdreconComponent },
    ],
  },
  { path: '', component: SignInComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
  public spinkit = Spinkit;
}
